import requests
import json
from headers import get_rooms_by_property_and_room_type_headers

propertyId = "03cf24de7e75-116f04065e08-0gg81dahht"
roomType = "SINGLE"

url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/room/getbypropertyandroomtype/" + propertyId + "/" + roomType

payload = ""
headers = get_rooms_by_property_and_room_type_headers.headers

response = requests.request("GET", url, data=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))
