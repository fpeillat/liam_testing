import requests
import json
from headers import delete_room_headers

roomId = "03cf24de7e75-116f04065e08-0gg81dahht-s04wem32"
url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/room/deletebyid/" + roomId

payload = ""
headers = delete_room_headers.headers

response = requests.request("DELETE", url, json=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))

assert response.json()['status'] == 200
