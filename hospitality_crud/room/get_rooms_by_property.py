import requests
import json
from authorisation import authorisation
from headers import get_room_by_property_headers

propertyId = "03cf24de7e75-116f04065e08-0gg81dahht"

url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/room/getbyproperty/" + propertyId

payload = ""
headers = get_room_by_property_headers.headers

response = requests.request("GET", url, data=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))
