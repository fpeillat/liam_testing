import requests
import json
from headers import create_room_headers
from payloads import create_room

propertyId = "03cf24de7e75-116f04065e08-0gg81dahht"
url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/room/create/" + propertyId

payload = create_room.payload
headers = create_room_headers.headers

response = requests.request("POST", url, json=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))

assert response.json()['status'] == 200


