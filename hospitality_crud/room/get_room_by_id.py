import requests
import json
from headers import get_room_by_id_headers

roomId = "03cf24de7e75-116f04065e08-25kwyqq2dx-xikbo9ia"

url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/room/getbyid/" + roomId

payload = ""
headers = get_room_by_id_headers.headers

response = requests.request("GET", url, json=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))

