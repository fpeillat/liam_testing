import requests
import json
from headers import update_room_headers
from payloads import update_room

propertyId = "03cf24de7e75-116f04065e08-0gg81dahht"

url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/room/update/" + propertyId

payload = update_room.payload
headers = update_room_headers.headers
response = requests.request("POST", url, json=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))

assert response.json()['status'] == 200
