import json
import random
import string

import pytest
import requests

from headers import header
from headers.header import (
    headers as id_headers,
    # we can add more here `xxx as yyy,`
)
from payloads import create_property, update_property, create_room, update_room

# Assignment change it for namedtuple or whichever you'd like to
# name = ...('...', [...])

property_id = None
room_id = None
host = 'https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/'


# OR start using fixtures that are outside the code
# @pytest.fixture(scope='...')
# def __change_me__():
#     ...


# add an external function to handle the post get delete methods
def post_payload(url, payload, headers, method: str = 'POST'):
    # headers_ = dict(property_create_headers.headers)
    args = dict(
        method=method.upper(),
        url=url,
        json=payload,
        headers=headers,
        timeout=10)
    rs = requests.request(**args)

    return rs


# def post_payload(**kwargs):
#     url = kwargs['url']
#     payload = kwargs['payload']
#     headers = kwargs['headers']
#     method = kwargs.get('method', 'POST')
#
#     rs = requests.request(method.upper(), url, json=payload, headers=headers)
#
#     return rs


# new config
# hospitality_crud.hospitality_crud_full_flow.TestHospitalityCRUD.test_create_property
class TestHospitalityCRUD:
    @pytest.fixture(scope='class')
    def prop_id(self):
        value = dict(property_id=None)
        return value

    @pytest.fixture(scope='class')
    def id_room(self):
        value = dict(room_id=None)
        return value

    def test_create_property(self, prop_id):
        args = dict(payload=create_property.payload,
                    url=header.url,
                    headers=id_headers,
                    method='POST', )

        response = post_payload(**args)

        # args = (
        #     property_create_headers.url,
        #     create_property.payload,
        #     prop_headers,
        #     'POST',)
        #
        # response = post_payload(*args)

        print(json.dumps(response.json(), indent=4, sort_keys=True))

        # extract the json() function to a common function ... maybe ...
        prop_id['property_id'] = response.json()['identifiers'][0]

        assert response.json()['status'] == 200
        # propertyId = self.propertyId

    def test_create_room(self, prop_id, id_room):
        url = f"{host}room/create/{prop_id['property_id']}"
        args = dict(payload=create_room.payload,
                    url=url,
                    headers=id_headers,
                    method='POST', )

        response = post_payload(**args)
        # response = post_payload(**args)
        # url = f"{host}/room/create/{tester_1['property_id']}"

        # payload = create_room.payload
        # headers = create_room_headers.headers
        #
        # response = requests.request("POST", url, json=payload, headers=headers)

        print(json.dumps(response.json(), indent=4, sort_keys=True))

        id_room['room_id'] = response.json()['identifiers'][0]
        # self.roomId = response.json()['identifiers'][0]
        # roomId = self.roomId
        assert response.json()['status'] == 200

        if response.json()['status'] == 200:
            print("Room Created, RoomId = " + str(id_room))

    def test_read_property(self, prop_id):
        url = f"{host}property/getbyid/{prop_id['property_id']}"
        args = dict(payload="",
                    url=url,
                    headers=id_headers,
                    method='GET', )

        response = post_payload(**args)

        print(json.dumps(response.json(), indent=4, sort_keys=True))

        if response.json()['url'] == "http://arvoia.com":
            print("Property Read, PropertyId = " + str(prop_id))

    def test_read_room(self, id_room):
        url = f"{host}room/getbyid/{id_room['room_id']}"
        args = dict(payload="",
                    url=url,
                    headers=id_headers,
                    method='GET', )

        response = post_payload(**args)

        print(json.dumps(response.json(), indent=4, sort_keys=True))

        if response.json()['images'][0] == "http://arvoia.com":
            print("Room Read, RoomId = " + str(id_room))

    def test_update_property(self, prop_id):
        url = f"{host}property/update/{prop_id['property_id']}"
        args = dict(payload=update_property.payload,
                    url=url,
                    headers=id_headers,
                    method='POST', )

        size = 4
        self.increment = ''.join(random.choices(string.ascii_uppercase, k=size))
        name = "Arvoia QA-LM" + str(self.increment)

        args[update_property.payload[0]['propertyId']] = prop_id
        args[update_property.payload[0]['name']] = name
        response = post_payload(**args)

        print(json.dumps(response.json(), indent=4, sort_keys=True))

        if response.json()['status'] == 200:
            print("Property Updated, PropertyId = " + str(prop_id) + " Updated Name = " + name)

    def test_update_room(self, prop_id, id_room):
        url = f"{host}room/update/{prop_id['property_id']}"
        args = dict(payload=update_room.payload,
                    url=url,
                    headers=id_headers,
                    method='POST', )

        size = 4
        self.increment = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase, k=size))
        name = "Arvoia QA-LM" + str(self.increment)

        payload = list(update_room.payload)  # casting to a new list
        payload[0]['id'] = id_room  # changing this to the fixture name
        payload[0]['propertyId'] = prop_id  # changing this to the fixture name
        payload[0]['name'] = name

        response = post_payload(**args)
        print(json.dumps(response.json(), indent=4, sort_keys=True))

        if response.json()['status'] == 200:
            print("Room Updated, RoomId = " + str(id_room) + " Updated Name = " + name)

    def test_delete_room(self, id_room):
        url = f"{host}room/deletebyid/{id_room['room_id']}"
        args = dict(payload="",
                    url=url,
                    headers=id_headers,
                    method='DELETE', )

        response = post_payload(**args)
        print(json.dumps(response.json(), indent=4, sort_keys=True))

        if response.json()['status'] == 200:
            print("Room Deleted")

    def test_delete_property(self, prop_id):
        url = f"{host}property/deletebyid/{prop_id['property_id']}"
        args = dict(payload="",
                    url=url,
                    headers=id_headers,
                    method='DELETE', )

        response = post_payload(**args)
        print(json.dumps(response.json(), indent=4, sort_keys=True))

        if response.json()['status'] == 200:
            print("Property Deleted")


class TestRoomCRUD:
    ...
