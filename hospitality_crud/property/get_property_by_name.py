import requests
import json
from authorisation import authorisation
from headers import get_property_by_name_headers

name = "Arvoia%20House-2"

url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/property/getbyname/" + name

payload = ""
headers = get_property_by_name_headers.headers

response = requests.request("GET", url, data=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))
