import requests
import json
from headers import header
from payloads import create_property

url = header.url

payload = create_property.payload
headers = header.headers

response = requests.request("POST", url, json=payload, headers=headers)


print(json.dumps(response.json(), indent=4, sort_keys=True))

propertyId = response.json()['identifiers'][0]

assert response.json()['status'] == 200

