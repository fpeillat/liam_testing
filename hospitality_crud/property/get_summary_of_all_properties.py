import requests
import json
from headers import get_summary_of_all_properties_headers

url = get_summary_of_all_properties_headers.url

payload = ""
headers = get_summary_of_all_properties_headers.headers

response = requests.request("GET", url, data=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))
