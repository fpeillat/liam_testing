import requests
import json
from headers import get_all_properties

url = get_all_properties.url

payload = ""
headers = get_all_properties.headers

response = requests.request("GET", url, data=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))
