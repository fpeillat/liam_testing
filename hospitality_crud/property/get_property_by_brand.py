import requests
import json
from headers import get_property_by_brand_headers

brand = "IT"

url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/property/getbybrand/IT" + brand

payload = ""
headers = get_property_by_brand_headers.headers

response = requests.request("GET", url, data=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))
