import requests
import json
from headers import get_property_by_identifier_headers

propertyId = "03cf24de7e75-116f04065e08-difddphtww"
url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/property/getbyid/" + propertyId

payload = ""
headers = get_property_by_identifier_headers.headers

response = requests.request("GET", url, data=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))
