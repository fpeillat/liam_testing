import requests
import json
from headers import property_update_headers
from payloads import update_property

url = property_update_headers.url

payload = update_property.payload
headers = property_update_headers.headers

response = requests.request("POST", url, json=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))

assert response.json()['status'] == 200
