import requests
import json
from headers import property_delete_headers

propertyId = "03cf24de7e75-116f04065e08-2vbvxhzytp"
url = "https://arvoia-gateway-qa-k8s.arvoia.com/hospitality/content/v1/property/deletebyid/" + propertyId

payload = ""
headers = property_delete_headers.headers

response = requests.request("DELETE", url, json=payload, headers=headers)

print(json.dumps(response.json(), indent=4, sort_keys=True))

assert response.json()['status'] == 200

