payload = [
    {
        "propertyId": "03cf24de7e75-116f04065e08-427nvdzyfr",
        "name": "Arvoia QA-LM",
        "brand": "IT",
        "url": "http://arvoia.com",
        "description": "Test property",
        "address": {
            "houseNumber": "1",
            "street": "Muckross Rd",
            "city": "Killarney",
            "country": "Ireland",
            "state": "Kerry",
            "province": "Munster",
            "countryCode": "IE",
            "postcode": "V93FJ68",
            "suburb": "Kerry"
        },
        "location": {
            "region": "south",
            "district": "KY",
            "openLocationCode": "string",
            "latitude": 0.0,
            "longitude": 0.0,
            "geoLocation": {
                "type": "Point",
                "coordinates": [
                    125.6,
                    10.1
                ]
            },
            "locationName": "Arvoia House",
            "city": "Killarney",
            "nearestAirport": "ORK"
        },
        "locationScore": "string",
        "starRating": 5,
        "reviewScore": None,
        "numberOfReviews": 103,
        "cleanlinessScore": 4.0,
        "mainImage": "http://arvoia.com",
        "images": [
            "http://arvoia.com"
        ],
        "photos": [
            "http://arvoia.com"
        ],
        "checkinTime": [
            8,
            0
        ],
        "checkoutTime": [
            16,
            30
        ],
        "accommodationType": "LUXURY",
        "numberOfRooms": 3,
        "hotelFacilities": [
            "SWIMMING_POOL"
        ],
        "isCCRequired": True,
        "defaultLanguage": "en",
        "currencyCode": "EUR",
        "breakfastReviewScore": 0.0,
        "petsAllowed": True,
        "isfreeCancellation": True,
        "noPrepaymentBlock": None
    }
]
