payload = (
    {
        "campaignId": "LM_Test_Property_2",
        "dataSource": {
            "sourceSystem": "http://www.example.com",
            "api": "hospitality personalised search",
            "action": "SEARCH",
            "affiliateId": "9e8a9190-4e97-409e-b59e-ea5093889df3",
            "userId": "90594422-fe86-4115-a308-aa8abd34fbe0",
            "searchReferenceId": "PROPERTY_SEARCHID_LM",
            "quoteReferenceId": "PROPERTY_QUOTEID_LM"
        },
        "user": {
            "country": "IE",
            "language": "EN-IE",
            "currency": "EUR",
            "ipAddress": "127.0.0.1",
            "email": "kgriffin@arvoia.com",
            "mobile": "+353871234567",
            "loyalty": {
                "signupDateTime": "2001-02-10T10:00:00",
                "expireDateTime": "2023-02-10T10:00:00",
                "effectiveDateTime": "2001-02-11T10:00:00",
                "loyaltyLevel": "Gold",
                "travelSector": "Hospitality",
                "membershipId": "12309719047",
                "vendorCode": "Hilton",
                "customerValue": "TEST_KTG",
                "programId": "Hilton Rewards"
            },
            "location": {
                "openLocationCode": "locationCode",
                "latitude": 9.79,
                "longitude": -52.23,
                "geoLocation": {
                    "type": "Point",
                    "coordinates": [
                        125.6,
                        10.1
                    ]
                },
                "locationName": "Hilton Killarney",
                "city": "Killarney",
                "district": "Co. Kerry",
                "region": "Munster",
                "country": {
                    "isoCode": "IE",
                    "isoNumeric": "372",
                    "fips": "EI",
                    "name": "Ireland",
                    "countryNameTranslated": {
                        "PT-BR": "Irlanda",
                        "DE": "Irland",
                        "FR": "Irlande"
                    },
                    "capital": "Dublin",
                    "tld": ".ie",
                    "currencyCode": "EUR",
                    "currencyName": "Euro",
                    "phone": "",
                    "postalCodeFormat": "XXX XXXX",
                    "postalCodeRegex": ".{3} .{4}",
                    "languages": [
                        "EN-IE",
                        "GA"
                    ],
                    "geonameid": "",
                    "neighbours": [
                        "Northern Ireland"
                    ],
                    "leadTime": 1,
                    "leadTimeHours": 1,
                    "vatRate": 21,
                    "mileageDefault": "Km",
                    "countryCallingCode": "+353",
                    "paymentCurrency": "EUR"
                },
                "iata": "HIL",
                "nearestAirport": "EIKY"
            }
        },
        "datesAndTimes": {
            "searchDateTime": "2021-03-04T08:30:00",
            "arrivalDateTime": "2021-04-10T10:00:00",
            "departureDateTime": "2021-04-17T10:00:00",
            "flexibleDates": True
        },
        "searchParameters": {
            "numberOfRooms": 1,
            "numberOfGuests": 1,
            "numberOfAdults": 1,
            "numberOfChildren": 0,
            "numberOfInfants": 0,
            "numberOfNights": 2,
            "promoCode": {
                "code": "PromoArvoia123",
                "type": "Promo"
            },
            "browseCurrency": "EUR",
            "travellingForWork": True,
            "acquisitionTerm": "cheap hotels near convention centre",
            "searchChannel": "ORGANIC",
            "acquisitionSource": "Source",
            "userDevice": "DESKTOP",
            "location": {
                "openLocationCode": "locationCode",
                "latitude": 9.798045,
                "longitude": -52.2346,
                "geoLocation": {
                    "type": "Point",
                    "coordinates": [
                        125.6,
                        10.1
                    ]
                },
                "locationName": "Hilton Killarney",
                "city": "Killarney",
                "district": "Co. Kerry",
                "region": "Munster",
                "country": {
                    "isoCode": "IE",
                    "isoNumeric": "372",
                    "fips": "EI",
                    "name": "Ireland",
                    "countryNameTranslated": {
                        "PT-BR": "Irlanda",
                        "DE": "Irland",
                        "FR": "Irlande"
                    },
                    "capital": "Dublin",
                    "tld": ".ie",
                    "currencyCode": "EUR",
                    "currencyName": "Euro",
                    "phone": "",
                    "postalCodeFormat": "XXX XXXX",
                    "postalCodeRegex": ".{3} .{4}",
                    "languages": [
                        "EN-IE",
                        "GA"
                    ],
                    "geonameid": "",
                    "neighbours": [
                        "Northern Ireland"
                    ],
                    "leadTime": 1,
                    "leadTimeHours": 1,
                    "vatRate": 21,
                    "mileageDefault": "Km",
                    "countryCallingCode": "+353",
                    "paymentCurrency": "EUR"
                },
                "iata": "HIL",
                "nearestAirport": "EIKY"
            }
        },
        "propertyDetails": [
            {
                "id": "03cf24de7e75-116f04065e08-0oucb0aj7w",
                "name": "Arvoia QA-LM-API-1-8-1",
                "brand": "IT",
                "url": "http://arvoia.com",
                "description": "Test property",
                "address": {
                    "houseNumber": "1",
                    "street": "Muckross Rd",
                    "suburb": "Kerry",
                    "city": "Killarney",
                    "country": "Ireland",
                    "countryCode": "IE",
                    "state": "Kerry",
                    "province": "Munster",
                    "floor": None,
                    "postcode": "V93FJ68",
                    "postcodeRegex": None
                },
                "location": {
                    "openLocationCode": "string",
                    "latitude": 0,
                    "longitude": 0,

                    "locationName": "Arvoia house",
                    "city": "Kerry",
                    "district": "Ky",
                    "region": "south",
                    "country": {
                        "isoCode": "IE",
                        "isoNumeric": "372",
                        "fips": "EI",
                        "name": "Ireland",
                        "countryNameTranslated": {
                            "PT-BR": "Irlanda",
                            "DE": "Irland",
                            "FR": "Irlande"
                        },
                        "capital": "Dublin",
                        "tld": ".ie",
                        "currencyCode": "EUR",
                        "currencyName": "Euro",
                        "phone": "",
                        "postalCodeFormat": "XXX XXXX",
                        "postalCodeRegex": ".{3} .{4}",
                        "languages": [
                            "EN-IE",
                            "GA"
                        ],
                        "geonameid": "",
                        "neighbours": [
                            "Northern Ireland"
                        ],
                        "leadTime": 1,
                        "leadTimeHours": 1,
                        "vatRate": 21,
                        "mileageDefault": "Km",
                        "countryCallingCode": "+353",
                        "paymentCurrency": "EUR"
                    },
                    "nearestAirport": "ORK"
                },
                "starRating": 5,
                "locationScore": "string",
                "reviewScore": 0.0,
                "numberOfReviews": 103,
                "cleanlinessScore": 4,
                "mainImage": "http://arvoia.com",
                "images": [
                    "http://arvoia.com"
                ],
                "photos": [
                    "http://arvoia.com"
                ],
                "checkoutTime": "16:30",
                "checkinTime": "08:00",
                "accommodationType": "BUDGET",
                "numberOfRooms": 3,
                "hotelFacilities": [
                    "SWIMMING_POOL"

                ],
                "isFreeCancellation": True,
                "isCCRequired": True,
                "isPrepaymentBlock": True,
                "defaultLanguage": "en",
                "currencyCode": "EUR",
                "minTotalPrice": 99.56,
                "breakfastReviewScore": 0,
                "petsAllowed": True,
                "margin": 98.11
            }
        ],
        "additionalMetadata": {
            "Notes": "Member is a VIP",
            "Notes2": "Member is a VIP2"
        }
    }
)
