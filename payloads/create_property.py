import time

dateTime = time.strftime('%d%b%Y%H%M%S')


payload = [
    {
        "name": "Arvoia QA-LM-API-" + dateTime,
        "brand": "IT",
        "url": "http://arvoia.com",
        "description": "Test property",
        "address": {
            "houseNumber": "1",
            "street": "Muckross Rd",
            "city": "Killarney",
            "country": "Ireland",
            "state": "Kerry",
            "province": "Munster",
            "countryCode": "IE",
            "postcode": "V93FJ68",
            "suburb": "Kerry"
        },
        "location": {
            "openLocationCode": "string",
            "latitude": 0,
            "longitude": 0,
            "region": "south",
            "geoLocation": {
                "type": "Point",
                "coordinates": [
                    125.2,
                    10.1
                ]
            },
            "locationName": "Arvoia house",
            "city": "Kerry",
            "district": "Ky",
            "nearestAirport": "ORK"
        },
        "locationScore": "string",
        "starRating": 5,
        "reviewScore": 0,
        "numberOfReviews": 103,
        "cleanlinessScore": 4,
        "mainImage": "http://arvoia.com",
        "images": [
            "http://arvoia.com"
        ],
        "photos": [
            "http://arvoia.com"
        ],
        "checkinTime": "08:00:00",
        "checkoutTime": "16:30:00",
        "accommodationType": "BUDGET",
        "numberOfRooms": 3,
        "hotelFacilities": [
            "SWIMMING_POOL"
        ],
        "IsCCRequired": True,
        "defaultLanguage": "en",
        "currencyCode": "EUR",
        "breakfastReviewScore": 0,
        "petsAllowed": True,
        "isfreeCancellation": True,
        "isPrepaymentBlock": True
    }
]
