payload = (
    {
        "dataSource": {
            "userId": "LM_TEST_BOOK",
            "searchReferenceId": "BOOK_SEARCHID_KTG",
            "quoteReferenceId": "BOOK_QUOTEID_KTG"
        },
        "bookingReferenceId": "20y72390f72f32-03f-0238f-203f8-008",
        "guestDetails": "guestDetails",
        "propertyId": "03cf24de7e75-116f04065e08-25kwyqq2dx",
        "roomId": "03cf24de7e75-116f04065e08-25kwyqq2dx-xikbo9ia",
        "bookingDateTime": "2021-03-09T10:00:00",
        "arrivalDateTime": "2021-05-05T10:00:00",
        "departureDateTime": "2021-05-12T10:00:00",
        "numberOfGuests": 1,
        "numberOfAdults": 1,
        "numberOfChildren": 0,
        "numberOfInfants": 0,
        "numberOfNights": 7,
        "totalBookingPrice": 700.00,
        "bookingPricePerNight": 100.00,
        "totalDepositPaid": 0.00,
        "paymentMethod": "PR",
        "paymentCurrency": "EUR",
        "isCancellable": False,
        "extras": [
            {
                "type": "room extra",
                "otaCode": 8,
                "price": 12.99,
                "currencyCode": "EUR",
                "quantity": 1,
                "description": "Jacuzzi"
            }
        ]
    }
)
