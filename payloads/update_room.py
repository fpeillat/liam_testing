payload = [
    {
        "roomId": "3cf24de7e75-116f04065e08-gdumzjsg0y-p6d81ew1",
        "propertyId": "03cf24de7e75-116f04065e08-25kwyqq2dx",
        "name": "QA-Main Meeting Room-LM",
        "type": "DOUBLE",
        "style": "FAMILY_FRIENDLY",
        "url": "http://arvoia.com/meetingroom2",
        "description": "Very friendly room",
        "beds": 1,
        "accommodationCategory": "HOTEL",
        "roomAmenities": [
            "KETTLE",
            "MINIBAR",
            "SAFE",
            "WIFI"
        ],
        "numberOfBathrooms": 1,
        "numberOfBedrooms": 0,
        "numberOfRooms": 1,
        "petsAllowed": True,
        "mainImage": "http://arvoia.com/static/meetingroom1.png",
        "images": [
            "http://arvoia.com"
        ],
        "maxCapacity": 4,
        "minCapacity": 1,
        "IsSmokingAllowed": False
    }
]
