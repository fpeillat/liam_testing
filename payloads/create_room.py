import time

dateTime = time.strftime('%d%b%Y%H%M%S')

payload = [
    {
        "id": "1_123_2_9_" + dateTime,
        "name": "QA-Main Meeting Room Three",
        "type": "SINGLE",
        "style": "FAMILY_FRIENDLY",
        "url": "http://arvoia.com/meetingroom1",
        "description": "Very friendly room",
        "beds": 1,
        "accommodationCategory": "HOTEL",
        "roomAmenities": [
            "KETTLE",
            "MINIBAR",
            "SAFE",
            "WIFI"
        ],
        "numberOfBathrooms": 1,
        "numberOfBedrooms": 2,
        "numberOfRooms": 1,
        "PetsAllowed": True,
        "mainImage": "http://arvoia.com/static/meetingroom1.png",
        "images": [
            "http://arvoia.com"
        ],
        "maxCapacity": 2,
        "minCapacity": 3,
        "isSmokingAllowed": True
    }
]
