payload = (
    {
        "campaignId  ": "LM_Test_Room",
        "dataSource  ": {
            "sourceSystem  ": "http://www.example.com",
            "api  ": "hospitality personalised search",
            "action  ": "SEARCH  ",
            "affiliateId  ": "9e8a9190-4e97-409e-b59e-ea5093889df3",
            "userId  ": "123e4567-e89b-12d3-a456-556642440000",
            "searchReferenceId  ": "ROOM_SEARCHID_LM",
            "quoteReferenceId  ": "ROOM_QUOTEID_LM"
        },
        "user  ": {
            "country": "IE",
            "language": "EN-IE",
            "currency": "EUR",
            "ipAddress": "127.0.0.1",
            "email": "kgriffin@arvoia.com",
            "mobile": "+353871234567",
            "loyalty": {
                "signupDateTime": "2001-02-10T10:00:00",
                "expireDateTime": "2023-02-10T10:00:00",
                "effectiveDateTime": "2001-02-11T10:00:00",
                "loyaltyLevel": "Gold",
                "travelSector": "Hospitality",
                "membershipId": "12309719047",
                "vendorCode": "Hilton",
                "customerValue": "TEST_ROOM_LM",
                "programId": "Hilton Rewards"
            },
            "location": {
                "openLocationCode": "locationCode",
                "latitude": 9.798045,
                "longitude": -52.2346,
                "geoLocation": {
                    "type": "Point",
                    "coordinates": [
                        125.6,
                        10.1
                    ]
                },
                "locationName": "Hilton Killarney",
                "city": "Killarney",
                "district": "Co. Kerry",
                "region": "Munster",
                "country": {
                    "isoCode": "IE",
                    "isoNumeric": "372",
                    "fips": "EI",
                    "name": "Ireland",
                    "countryNameTranslated": {
                        "PT-BR": "Irlanda",
                        "DE": "Irland",
                        "FR": "Irlande"
                    },
                    "capital": "Dublin",
                    "tld": ".ie  ",
                    "currencyCode": "EUR",
                    "currencyName": "Euro",
                    "phone": "",
                    "postalCodeFormat": "XXX XXXX",
                    "postalCodeRegex": ".{3} .{4}",
                    "languages": [
                        "EN-IE",
                        "GA"
                    ],
                    "geonameid": "",
                    "neighbours": [
                        "North of Ireland"
                    ],
                    "leadTime": 1,
                    "leadTimeHours": 1,
                    "vatRate": 21,
                    "mileageDefault": "Km",
                    "countryCallingCode": "+353",
                    "paymentCurrency": "EUR"
                },
                "iata": "HIL",
                "nearestAirport": "EIKY"
            }
        },
        "datesAndTimes": {
            "searchDateTime": "2021-03-03T10:00:00",
            "arrivalDateTime": "2021-05-05T10:00:00",
            "departureDateTime": "2021-05-12T10:00:00",
            "flexibleDates": False,
            "defaultBookingDates": False
        },
        "searchParameters": {
            "numberOfRooms": 1,
            "numberOfGuests": 1,
            "numberOfAdults": 1,
            "numberOfChildren": 0,
            "numberOfInfants": 0,
            "numberOfNights": 2,
            "promoCode": {
                "code": "PromoArvoia123",
                "type": "Promo"
            },
            "browseCurrency": "EUR",
            "travellingForWork": True,
            "acquisitionTerm": "cheap hotels near convention centre",
            "searchChannel": "ORGANIC",
            "acquisitionSource": "Source",
            "userDevice": "DESKTOP",
            "location": {
                "openLocationCode": "locationCode",
                "latitude": 9.798045,
                "longitude": -52.2346,
                "geoLocation": {
                    "type": "Point",
                    "coordinates": [
                        125.6,
                        10.1
                    ]
                },
                "locationName": "Hilton Killarney",
                "city": "Killarney",
                "district": "Co. Kerry",
                "region": "Munster",
                "country": {
                    "isoCode": "IE",
                    "isoNumeric": "372",
                    "fips": "EI",
                    "name": "Ireland",
                    "countryNameTranslated": {
                        "PT-BR": "Irlanda",
                        "DE": "Irland",
                        "FR": "Irlande"
                    },
                    "capital": "Dublin",
                    "tld": ".ie  ",
                    "currencyCode": "EUR",
                    "currencyName": "Euro",
                    "phone": "",
                    "postalCodeFormat": "XXX XXXX",
                    "postalCodeRegex": ".{3} .{4}",
                    "languages": [
                        "EN-IE",
                        "GA"
                    ],
                    "geonameid": "",
                    "neighbours": [
                        "North of Ireland"
                    ],
                    "leadTime": 1,
                    "leadTimeHours": 1,
                    "vatRate": 21,
                    "mileageDefault": "Km",
                    "countryCallingCode": "+353",
                    "paymentCurrency": "EUR"
                },
                "iata": "HIL",
                "nearestAirport": "EIKY"
            }
        },
        "roomDetails": [
            {
                "id": "03cf24de7e75-116f04065e08-iic7vy6pet-vkphktwn",
                "propertyId": "property-id-lm",
                "name": "Executive Suite",
                "type": "SUITE",
                "style": "BUSINESS",
                "url": "http://www.hiltonkillarney/execsuite",
                "description": "Executive fancypants suite",
                "beds": 2,
                "occupancy": 4,
                "accommodationCategory ": "HOTEL",
                "roomAmenities": [
                    "KETTLE",
                    "MINIBAR",
                    "SAFE",
                    "WIFI"
                ],
                "numberOfBathrooms": 1,
                "numberOfBedrooms": 2,
                "numberOfRooms": 2,
                "petsAllowed": True,
                "numberOfAdults": 2,
                "numberOfChildren": 1,
                "numberOfInfants": 0,
                "mainImage": "http://cdn.hilton.com/exec",
                "images": [
                    "http://cdn.hilton.com/pic1  ",
                    "http://cdn.hilton.com/pic2  ",
                    "http://cdn.hilton.com/pic3  "
                ],
                "maxCapacity": 3,
                "minCapacity": 1,
                "paymentAmount": 98.76,
                "paymentModel": "PR",
                "paymentCurrency": "EUR",
                "paymentDepositRequired": False,
                "pricePerNight": 51.20,
                "isSmokingAllowed": True,
                "reviewScore": 4,
                "rateCode": "DIS1",
                "rateModifiers": [
                    "LOYALTY",
                    "FREQUENT_BOOKER"
                ]
            }
        ],
        "extras  ": [
            {
                "type": "room extra",
                "otaCode": 8,
                "price": 12.99,
                "currencyCode": "EUR",
                "quantity": 1,
                "description": "Wine"
            }
        ],
        "additionalMetadata": {
            "Notes": "Member is a VIP",
            "Notes2": "Member is a VIP2"
        }
    }
)
