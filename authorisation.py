import requests
import json

url = "https://arvoia-gateway-qa-k8s.arvoia.com/signin"

payload = {
    "username": "qa@mobacar.com",
    "password": "Password1?"
}
headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
    'Postman-Token': "9c743b8c-a571-41dd-8522-2ea659d84e4e"
}

response = requests.request("POST", url, json=payload, headers=headers)
print(json.dumps(response.json()['authenticationResult']['idToken'], indent=4, sort_keys=True))
authorisation = response.json()['authenticationResult']['idToken']
